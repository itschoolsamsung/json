package com.example.al.json;

import java.util.List;

public class Weather {
    class Query {
        class Results {
            class Channel {
                class Units {
                    String distance;
                    String pressure;
                    String speed;
                    String temperature;
                }
                class Item {
                    class Forecast {
                        int code;
                        String date;
                        String day;
                        int high;
                        int low;
                        String text;
                    }

                    List<Forecast> forecast;

                    @Override
                    public String toString() {
                        String result = "";

                        for (Forecast f : forecast) {
                            result += f.date + " (" + f.low + " - " + f.high + ") " + f.text + "\n";
                        }

                        return result;
                    }
                }

                Units units;
                String title;
                Item item;
            }

            Channel channel;
        }

        int count;
        String created;
        String lang;
        Results results;
    }

    Query query;
}
