package com.example.al.json;

import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.google.gson.Gson;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	EditText editText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editText = findViewById(R.id.editText);
		findViewById(R.id.button1).setOnClickListener(this);
		findViewById(R.id.buttonAll).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.button1:
				new AsyncRequest1().execute();
				break;
			case R.id.buttonAll:
				new AsyncRequestAll().execute();
				break;
		}
	}

	class Person {
		int id;
		int id_user;
		String userName;
		int date;
		String text;
	}

	class Messages {
		Person[] messages;
	}

	class AsyncRequest1 extends AsyncTask {
		StringBuilder sb = new StringBuilder();

		@Override
		protected Object doInBackground(Object[] objects) {
			HttpURLConnection urlConnection = null;

			try {
//				URL url = new URL("http://samsung.avalonstudio.ru/json0.php?id=1");
				URL url = new URL("http://samsung.avalonstudio.ru/api/message/view?id=1");
//				URL url = new URL("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid=2086230&format=json");
				urlConnection = (HttpURLConnection)url.openConnection();
				InputStream is = urlConnection.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				int data;
				while ((data = isr.read()) != -1) {
					sb.append((char)data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (urlConnection != null) {
					urlConnection.disconnect();
				}
				Log.i("__JSON__", sb.toString());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Object o) {
			String result = sb.toString();

			Gson gson = new Gson();
			Person p = gson.fromJson(result, Person.class);
			editText.setText(String.format("id=%d\nuserName=%s\ntext=%s\n\n", p.id, p.userName, p.text));
		}
	}

	class AsyncRequestAll extends AsyncTask {
		StringBuilder sb = new StringBuilder();

		@Override
		protected Object doInBackground(Object[] objects) {
			HttpURLConnection urlConnection = null;

			try {
//				URL url = new URL("http://samsung.avalonstudio.ru/json0.php");
				URL url = new URL("http://samsung.avalonstudio.ru/api/message/index");
				urlConnection = (HttpURLConnection)url.openConnection();
				InputStream is = urlConnection.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				int data;
				while ((data = isr.read()) != -1) {
					sb.append((char)data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (urlConnection != null) {
					urlConnection.disconnect();
				}
				Log.i("__JSON__", sb.toString());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Object o) {
			String result = sb.toString();

			Gson gson = new Gson();
			Messages msg = gson.fromJson(result, Messages.class);
			editText.setText("");
			for (Person p : msg.messages) {
				editText.append(String.format("id=%d\nuserName=%s\ntext=%s\n\n", p.id, p.userName, p.text));
			}
		}
	}
}
